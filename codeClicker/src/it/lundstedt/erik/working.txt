package it.lundstedt.erik;

import com.google.gson.Gson;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class main {

    public static void main(String[] args) throws IOException {
    	System.out.println("Hello World!");

    	/*writeDataToCfg("test",10);
	    for (int i = 0; i < 10; i++) {
		    writeDataToCfg("test"+i,i);
	    }
    	*/


    	Gson json=new Gson();
        SimpleUser user=new SimpleUser(19,"erik");
        String text=json.toJson(user);
	    System.out.println(text);


	    System.out.println(	    json.toJson(user.name));
	    System.out.println(	    json.toJson(user.age));


	    File settings=new File("data.json");
	    FileWriter writer =new FileWriter(settings);
	    FileReader reader=new FileReader(settings);
	    writer.write(text);
	    writer.close();

	    System.out.println(reader.toString());



	    System.out.println(json.toJson(user));
    }



	public static void writeDataToCfg(String key,long value)
	{
		JSONObject obj = new JSONObject();
		obj.put(key, value);
		try {
			FileWriter file = new FileWriter("data.json");
			file.write(obj.toJSONString());
			file.flush();
			file.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.print(obj.toJSONString());
	}



}
